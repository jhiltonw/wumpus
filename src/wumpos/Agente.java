/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wumpos;

import java.util.Random;
import javax.swing.JOptionPane;

/**
 *
 * @author Hilton
 */
public class Agente {
    Constroi Construir = new Constroi();
    Mostrar Print = new Mostrar();
    private String tabuleiro[][] = new String[4][4];
    String move[][] = new String[4][4];
    Random R = new Random();

    public Agente(String tabu[][]) {
    this.tabuleiro = tabu;
    }

    

    boolean life = true;
    int flecha = 1;
    int Gold = 0;
    
    int Mlinha = 0;
    int Mcoluna = 0;

    
    public void criarTMove(){
        for (int i = 0; i < move.length; i++) {
         
            for (int j = 0; j < move[i].length; j++) {               
                
                if (Mlinha == i && Mcoluna == j) {
                    move[i][j] = "******";                   
                }
                else{
                    move[i][j] = "      ";
                }
            }
        }
        Print.mostrar2(tabuleiro, move);
        
    }
    
    
    public void andarFrente(){
        
        if(Mcoluna < 3){
            Mcoluna++;
            criarTMove();
            System.out.println("Andou para Frente");
        }
        if(tabuleiro[Mlinha][Mcoluna].equals("-Ouro-") || tabuleiro[Mlinha][Mcoluna].equals("^FOuro") || tabuleiro[Mlinha][Mcoluna].equals("FBOuro") || tabuleiro[Mlinha][Mcoluna].equals("BOuro-")){
                Gold++;
                tabuleiro[Mlinha][Mcoluna] = "      ";
        }
        else if(tabuleiro[Mlinha][Mcoluna].equals("Wumpus") || tabuleiro[Mlinha][Mcoluna].equals("Poco  ")){
            JOptionPane.showMessageDialog(null, "Lose");
            life = false;
            System.runFinalization();
        }
    }
    
    public void andarTraz(){
        
        if(Mcoluna > 0){
            Mcoluna--;
            criarTMove();
            System.out.println("Andou para Traz");
        }
        if(tabuleiro[Mlinha][Mcoluna].equals("-Ouro-") || tabuleiro[Mlinha][Mcoluna].equals("^FOuro") || tabuleiro[Mlinha][Mcoluna].equals("FBOuro") || tabuleiro[Mlinha][Mcoluna].equals("BOuro-")){
                Gold++;
                tabuleiro[Mlinha][Mcoluna] = "      ";
        }
        else if(tabuleiro[Mlinha][Mcoluna].equals("Wumpus") || tabuleiro[Mlinha][Mcoluna].equals("Poco  ")){
            JOptionPane.showMessageDialog(null, "Lose");
            life = false;
            System.runFinalization();
        }
    }
    
    public void andarBaixo(){
        
        if(Mlinha < 3){
            Mlinha++;
            criarTMove();
            System.out.println("Andou para Baixo");
        }
        if(tabuleiro[Mlinha][Mcoluna].equals("-Ouro-") || tabuleiro[Mlinha][Mcoluna].equals("^FOuro") || tabuleiro[Mlinha][Mcoluna].equals("FBOuro") || tabuleiro[Mlinha][Mcoluna].equals("BOuro-")){
                Gold++;
                tabuleiro[Mlinha][Mcoluna] = "      ";
        }
        else if(tabuleiro[Mlinha][Mcoluna].equals("Wumpus") || tabuleiro[Mlinha][Mcoluna].equals("Poco  ")){
            JOptionPane.showMessageDialog(null, "Lose");
            life = false;
            System.runFinalization();
        }
    }
    
    public void andarCima(){
        
        if(Mlinha > 0){
            Mlinha--;
            criarTMove();
            System.out.println("Andou para Cima");
        }
        if(tabuleiro[Mlinha][Mcoluna].equals("-Ouro-") || tabuleiro[Mlinha][Mcoluna].equals("^FOuro") || tabuleiro[Mlinha][Mcoluna].equals("FBOuro") || tabuleiro[Mlinha][Mcoluna].equals("BOuro-")){
                Gold++;
                tabuleiro[Mlinha][Mcoluna] = "      ";
        }
        else if(tabuleiro[Mlinha][Mcoluna].equals("Wumpus") || tabuleiro[Mlinha][Mcoluna].equals("Poco  ")){
            JOptionPane.showMessageDialog(null, "Lose");
            life = false;
            System.runFinalization();
        }
    }
    
    public void Caminhar(){
        int obsFrente = 0;
        int obsTraz = 0;
        int obsCima = 0;
        int obsBaixo = 0;
        int random =0 ;
        boolean largada = true;
        while(life){
            if(Gold == 1){
                Voltar();
                break;
            }
            //Start
            else if(largada == true){
                random = R.nextInt(2);
                if(random == 0){
                    andarFrente();
                    largada = false;
                    if(!tabuleiro[Mlinha][Mcoluna].equals("      ")){
                    andarTraz();
                    
                    }
                }
                else{
                    andarBaixo();
                    largada = false;
                    if(!tabuleiro[Mlinha][Mcoluna].equals("      ")){
                    andarCima();
                    
                    }
                }
            }//////////////////////////
            else if(life){
            //inicio    
                if(obsFrente ==0 && obsTraz ==0 && obsCima ==0 && obsBaixo ==0){
                    if(Mcoluna < 3){
                        andarFrente();
                    }
                    else{
                        andarBaixo(); 
                    }if(!tabuleiro[Mlinha][Mcoluna].equals("      ")){
                        andarTraz();
                        obsFrente++;
                    }
                    
               
                }else if(obsFrente == 1 && obsTraz == 0 && obsCima == 0 && obsBaixo == 0){
                    if(Mlinha > 0){
                        andarCima();
                    }else if(Mcoluna > 0){
                        andarTraz(); 
                    }else{
                        andarBaixo();
                    }
                    if(!tabuleiro[Mlinha][Mcoluna].equals("      ") ){
                        andarBaixo();
                        obsCima++;
                    }
               
                }else if(obsFrente ==1 && obsTraz ==0 && obsCima ==1 && obsBaixo == 0){
                    if(Mlinha < 3){
                        andarBaixo();
                    }else if(Mcoluna > 0){
                        andarTraz(); 
                    }else{
                        andarFrente();
                    }
                    if(!tabuleiro[Mlinha][Mcoluna].equals("      ") ){
                        andarCima();
                        obsBaixo++;
                        
                    }
               
                }else if(obsFrente == 1 && obsTraz == 0 && obsCima == 1 && obsBaixo == 1){
                    if(Mcoluna > 0){
                        andarTraz();
                    }else{
                        andarBaixo(); 
                    }
                    if(!tabuleiro[Mlinha][Mcoluna].equals("      ")){
                        andarFrente();
                        obsTraz++;
                    }
               
                }
                // se frente e baixo
                else if(obsFrente == 1 && obsTraz == 0 && obsCima == 0 && obsBaixo == 1){
                    if(Mlinha > 0){
                        andarTraz();
                        
                    }else{
                        andarFrente(); 
                    }
                    if(!tabuleiro[Mlinha][Mcoluna].equals("      ") ){
                        andarBaixo();
                        obsTraz++;
                    }
               
                }// frente, baixo e traz
                else if(obsFrente == 1 && obsTraz == 1 && obsCima == 0 && obsBaixo == 1){
                    if(Mlinha > 0){
                        andarCima();
                        
                    }else{
                        andarFrente(); 
                    }
                    if(!tabuleiro[Mlinha][Mcoluna].equals("      ") || Mlinha ==0){
                        andarBaixo();
                        obsCima++;
                    }
               
                }
                //baixo
                else if(obsFrente == 0 && obsTraz == 0 && obsCima == 0 && obsBaixo == 1){
                    if(Mlinha > 0){
                        andarCima();
                        
                    }else{
                        andarFrente(); 
                    }
                    if(!tabuleiro[Mlinha][Mcoluna].equals("      ") || Mlinha ==0){
                        andarBaixo();
                        obsCima++;
                    }
               
                }
                // se todos os lados
                else if(obsFrente == 1 && obsTraz == 1 && obsCima == 1 && obsBaixo == 1){
                    if(Mcoluna > 0){
                        andarFrente();
                        obsFrente = 0;
                        obsTraz = 0;
                        obsCima = 0;
                        obsBaixo = 0;
                    }
                    
               
                }
           
            
            //fim
            }
            
            
            
        }
    }
    
    public void Voltar(){
        while(Gold == 1){
            if(Mlinha !=0 && (!tabuleiro[Mlinha-1][Mcoluna].equals("Wumpus") && !tabuleiro[Mlinha-1][Mcoluna].equals("Poco  "))){
                Mlinha--;
                criarTMove();
            }else if(Mcoluna != 0 &&(Mlinha ==0 && (!tabuleiro[Mlinha][Mcoluna-1].equals("Wumpus") && !tabuleiro[Mlinha][Mcoluna-1].equals("Poco   ")))){
                Mcoluna--;
                criarTMove();
            }
            else if(Mlinha == 0 && Mcoluna == 0){
                criarTMove();
                JOptionPane.showMessageDialog(null, "Win!");
                break;
            }
        }
        
        
    }
    
}
